<?php
/**
 * Eurotax importer plugin for Craft CMS 3.x
 *
 * Import Plugin for Eurotax Cars
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

/**
 * @author    Snapdesign AG
 * @package   EurotaxImporter
 * @since     1.0.0
 */
return [
    'Eurotax importer plugin loaded' => 'Eurotax importer plugin loaded',
];
