<?php

namespace snapdesign\eurotaximporter\models;

use craft\base\Model;

class Settings extends Model
{
    public $pathToExport = '';
    public $pathToPictures = '';
    public $sectionHandle = '';
    public $folderName = '';

    public function rules()
    {
        return [
            [['pathToExport', 'pathToPictures', 'sectionHandle', 'folderName'], 'required'],
            // ...
        ];
    }
}