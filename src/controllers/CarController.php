<?php
/**
 * Eurotax importer plugin for Craft CMS 3.x
 *
 * Import Plugin for Eurotax Cars
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

namespace snapdesign\eurotaximporter\controllers;

use Craft;
use craft\web\Controller;
use snapdesign\eurotaximporter\EurotaxImporter;

/**
 * @author    Snapdesign AG
 * @package   EurotaxImporter
 * @since     1.0.0
 */
class CarController extends Controller
{
    public function actionImport()
    {
        $this->requirePostRequest();
        $ids = Craft::$app->request->getBodyParam('ids');
        if($ids){
            EurotaxImporter::getInstance()->cars->importCars($ids);
        }
    }
}
