<?php
/**
 * Eurotax importer plugin for Craft CMS 3.x
 *
 * Import Plugin for Eurotax Cars
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

namespace snapdesign\eurotaximporter\services;

use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\elements\Entry;
use SimpleXMLElement;
use snapdesign\eurotaximporter\EurotaxImporter;

/**
 * @author    Snapdesign AG
 * @package   EurotaxImporter
 * @since     1.0.0
 */
class Cars extends Component
{
    protected $settings;

    protected $pathToExport;

    protected $section;

    protected $cars = [];

    protected $eurotaxIdOfimportedCars = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->settings = EurotaxImporter::getInstance()->getSettings();

        $this->checkForNewExport();

        $this->section = Craft::$app->sections->getSectionByHandle($this->settings->sectionHandle);
        $this->eurotaxIdOfimportedCars = $this->loadEurotaxIdOfImportedCars();
        $this->cars = $this->loadCars();
    }

    protected function checkForNewExport()
    {
        $pathToArchive = pathinfo($this->settings->pathToExport)['dirname'] . "/export.zip";
        if (file_exists($pathToArchive)) {
            $zip = new \ZipArchive();
            $zip->open($pathToArchive);

            //Delete all old images
            array_map('unlink', glob("{$this->settings->pathToPictures}/*.*"));
            //Create pictures Folder if doesent exists
            if (!file_exists($this->settings->pathToPictures)) {
                mkdir($this->settings->pathToPictures, 0777, true);
            }

            //Unzip all files
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $filename = $zip->getNameIndex($i);
                switch (true) {
                    case (pathinfo($filename)['extension'] ?? '') == 'xml':
                        $xml = new SimpleXMLElement((string)$zip->getFromName($filename));
                        $xml->asXML($this->settings->pathToExport);
                        break;
                    default:
                        file_put_contents($this->settings->pathToPictures . $filename, $zip->getFromName($filename));
                }
            }

            $archivePathinfo = pathinfo($pathToArchive);
            rename(
                $pathToArchive,
                $archivePathinfo['dirname'] . "/" . "last_export.zip"
            );
        }
    }

    protected function loadEurotaxIdOfImportedCars()
    {
        return array_map(function ($item) {
            return $item->carEurotaxId;
        }, Entry::findAll(['sectionId' => $this->section->id, 'siteId' => 5]));
    }

    protected function loadCars()
    {
        $xml = simplexml_load_file($this->settings->pathToExport);

        $cars = [];

        foreach ($xml->FZ->TYP as $carXml) {
            $eurotaxId = (string)$carXml->xpath('./@TYPLAUFNR')[0];
            $firtsRegistration = (string)$carXml->TYPERSTIV;
            $firtsRegistration = !empty($firtsRegistration) ? \DateTime::createFromFormat('Ymd', $firtsRegistration)->format('d.m.Y') : '';
            $cars[$eurotaxId] = [
                'carBrand' => (string)$carXml->TYPMARBEZ,
                'carModel' => (string)$carXml->TYPBEZ,
                'carConstructionDate' => (string)$carXml->TYPMODBJ,
                'carChassisNumber' => (string)$carXml->TYPFZGNR,
                'carFirstRegistration' => $firtsRegistration,
                'carMileage' => (string)$carXml->TYPKMIST,
                'carConstruction' => (string)$xml->xpath('./TXT/EINTRAG[@SPR="SG"][@NUMMER="' . (string)$carXml->TYPTXTAUFBAUCD2 . '"]/@TEXT')[0],
                'carNumberOfDoors' => (string)$carXml->TYPANZTUER,
                'carNumberOfGears' => (string)$carXml->TYPANZGAENGE,
                'carNumberOfSeats' => (string)$carXml->TYPANZSITZ,
                'carCapacity' => (string)$carXml->TYPHUBRAUM,
                'carPs' => (string)$carXml->TYPPS,
                'carKw' => (string)$carXml->TYPKW,
                'carConsumptionTotal' => (string)$carXml->TYPVERBGES,
                'carConsumptionCity' => (string)$carXml->TYPVERBSTADT,
                'carConsumptionLand' => (string)$carXml->TYPVERBLAND,
                'carCo2' => (string)$carXml->TYPCO2,
                'carCo2Gas' => (string)$carXml->TYPCO2GAS,
                'carColor' => (string)$carXml->TYPFARBE,
                'carInteriorDecoration' => (string)$carXml->TYPPOLSTER,
                'carFrameNumber' => (string)$carXml->TYPFZGNR,
                'carTotalWeight' => (string)$carXml->TYPGESGEWICHT,
                'carPayload' => (string)$carXml->TYPNUTZLAST,
                'careTare' => (string)$carXml->TYPLEERGEWICHT,
                'carRoofLoad' => (string)$carXml->TYPDACHLAST,
                'carWheelbase' => (string)$carXml->TYPRADSTAND,
                'carTrailerLoadBraked' => (string)$carXml->TYPANHLASTGEB,
                'carTrailerLoadUnbraked' => (string)$carXml->TYPANHLASTUNGEB,
                'carTrunkVolumeFrom' => (string)$carXml->TYPKOFRMVOLVON,
                'carTrunkVolumeTo' => (string)$carXml->TYPKOFRMVOLBIS,
                'carGroundClearance' => (string)$carXml->TYPBODFREIH,
                'carTankCapacity' => (string)$carXml->TYPTANKINHALT,
                'carTankCapacityGas' => (string)$carXml->TYPTANKINHALTGAS,
                'carTorque' => (string)$carXml->TYPDREHMOMENT,
                'carRpm' => (string)$carXml->TYPDREHZAHL,
                'carLengthFrom' => (string)$carXml->TYPLAENGEVON,
                'carLengthTo' => (string)$carXml->TYPLAENGEBIS,
                'carWidthFrom' => (string)$carXml->TYPBREITEVON,
                'carWidthTo' => (string)$carXml->TYPBREITEBIS,
                'carHeightFrom' => (string)$carXml->TYPHOEHEVON,
                'carHeightTo' => (string)$carXml->TYPHOEHEBIS,
                'carMaxSpeed' => (string)$carXml->TYPGESCHWMAX,
                'carMaxSpeedGas' => (string)$carXml->TYPGESCHWMAXGAS,
                'carPowerUnit' => (string)$xml->xpath('./TXT/EINTRAG[@SPR="SG"][@NUMMER="' . (string)$carXml->TYPTXTANTRCD2 . '"]/@TEXT')[0],
                'carTypeCertificate' => (string)$carXml->TYPTYPSCHEIN,
                'carEurotaxId' => $eurotaxId,
                'carGear' => (string)$carXml->MERKMALE->SCHALTGETRIEBE == 1 ? 'geschaltet' : (string)$carXml->MERKMALE->AUTOMATIKGETRIEB == 1 ? 'automat' : '',
                'carSeries' => trim(str_replace(' SERIES', 'er', (string)$carXml->TYPMODGRP1)),
                'carEngine' => (function ($xml, $carXml) {
                    $carEngine = (string)$xml->xpath('./TXT/EINTRAG[@SPR="SG"][@NUMMER="' . (string)$carXml->TYPTXTTRBSTCD2 . '"]/@TEXT')[0];
                    $carEngine = strtolower($carEngine);
                    switch (true) {
                        case strpos($carEngine, 'elektrisch') !== false:
                            return 'elektro';
                            break;
                        case strpos($carEngine, 'benzin') !== false:
                            return 'benzin';
                            break;
                        case strpos($carEngine, 'diesel') !== false:
                            return 'diesel';
                            break;
                    }

                })($xml, $carXml),
                'carStandardEquipment' => array_map(function ($item) {
                    return ['col1' => (string)$item];
                }, $carXml->xpath('./SERIE/SAPOS/SAPTEXT[@SPR="SG"]')),
                'carOptionalEquipment' => (function ($carXml) {
                    $items = [];
                    foreach ($carXml->xpath('./SONDER/SAPOS') As $index => $sonderausstattung) {
                        $index++;
                        $items["new{$index}"] = [
                            'type' => 'BlockCarOptionalEquipment',
                            'fields' => [
                                'blockName' => (string)$sonderausstattung->xpath('./SAPTEXT[@SPR="SG"]')[0],
                                'blockContent' => array_map(function ($item) {
                                    return ['col1' => (string)$item];
                                }, $sonderausstattung->xpath('./INHALT/SAITEXT[@SPR="SG"]'))
                            ]
                        ];
                    }
                    return $items;
                })($carXml),
                'pictures' => array_map(function ($item) {
                    return (string)$item;
                }, $carXml->xpath('./PICTURES/PIC')),
            ];
        }

        return $cars;
    }

    public function getCars()
    {
        return array_map(function ($item) {
            $item['alreadyImported'] = in_array($item['carEurotaxId'], $this->eurotaxIdOfimportedCars);
            return $item;
        }, $this->cars);
    }

    public function getCar($id)
    {
        return $this->cars[$id];
    }

    protected function uploadImage($originalPath, $filename = null)
    {
        //https://craftcms.stackexchange.com/questions/20965/how-do-i-upload-an-asset-in-craft-3-via-php

        $folder = array_values(Craft::$app->getAssets()->findFolders(['name' => $this->settings->folderName]))[0];

        $pathInfo = pathinfo($originalPath);
        $tempPath = $pathInfo['dirname'] . "/" . $pathInfo['filename'] . "_temp." . $pathInfo['extension'];

        $filename = $filename ?? $pathInfo['basename'];

        copy($originalPath, $tempPath);
        $asset = new Asset();
        $asset->tempFilePath = $tempPath;
        $asset->filename = $filename;
        $asset->avoidFilenameConflicts = true;
        $asset->newFolderId = $folder->id;
        $asset->volumeId = $folder->volumeId;
        $asset->setScenario(Asset::SCENARIO_CREATE);

        $result = Craft::$app->getElements()->saveElement($asset);

        return $asset->id;
    }

    public function importCars($ids = [])
    {
        $entryTypes = $this->section->getEntryTypes();
        $entryType = reset($entryTypes);
        foreach ($ids as $id) {
            $carData = $this->getCar($id);

            $carData['carImages'] = [];
            $imagename = "{$carData['carBrand']}_{$carData['carModel']}_{$carData['carColor']}";
            foreach ($carData['pictures'] as $picture) {
                $imagename .= '.' . pathinfo($picture)['extension'];
                $carData['carImages'][] = $this->uploadImage($this->settings->pathToPictures . $picture, $imagename);
            }
            unset($carData['pictures']);

            $entry = new Entry([
                'sectionId' => $this->section->id,
                'typeId' => $entryType->id,
                'fieldLayoutId' => $entryType->fieldLayoutId,
                'authorId' => Craft::$app->user->id,
                'siteId' => 5//@todo cahnge
            ]);
            $entry->setFieldValues($carData);
            Craft::$app->elements->saveElement($entry);
        }
    }
}
