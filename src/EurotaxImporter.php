<?php
/**
 * Eurotax importer plugin for Craft CMS 3.x
 *
 * Import Plugin for Eurotax Cars
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

namespace snapdesign\eurotaximporter;


use Craft;
use craft\base\Plugin;
use craft\events\PluginEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\services\Plugins;
use craft\web\twig\variables\CraftVariable;
use craft\web\UrlManager;
use snapdesign\eurotaximporter\models\Settings;
use snapdesign\eurotaximporter\services\Cars;
use yii\base\Event;

/**
 * Class EurotaxImporter
 *
 * @author    Snapdesign AG
 * @package   EurotaxImporter
 * @since     1.0.0
 *
 */
class EurotaxImporter extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var EurotaxImporter
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'cars' => Cars::class,
        ]);

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['POST eurotax/import'] = 'eurotax-importer/car/import';
            }
        );

        Event::on(CraftVariable::class, CraftVariable::EVENT_INIT, function (Event $e) {
            $variable = $e->sender;
            $variable->set('eurotaxCars', Cars::class);
        });

        Craft::info(
            Craft::t(
                'eurotax-importer',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    protected function createSettingsModel()
    {
        return new Settings();
    }

    protected function settingsHtml()
    {
        return \Craft::$app->getView()->renderTemplate('eurotax-importer/settings', [
            'settings' => $this->getSettings()
        ]);
    }

}
